---
title: Example Report
author: Peter Alping
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at elit in est dictum molestie vitae sed nisi. Duis a tortor id erat aliquet malesuada. Quisque efficitur est viverra, accumsan turpis vel, dapibus purus. Nullam at metus lacus. Duis iaculis libero sit amet iaculis dignissim.

# Subsection

Nam at nisi quis nisi congue ornare facilisis nec nisi. Ut sit amet auctor nisi. Suspendisse nec eros nec felis finibus venenatis ut non velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean pretium elementum magna non volutpat. In non velit quis lacus ultrices convallis. Nunc velit sem, scelerisque ut pulvinar mollis, efficitur eget lacus. Nam accumsan neque ac magna consequat vestibulum. Maecenas nisi dolor, scelerisque eu pellentesque eu, congue non eros.

# Figure

![Figure: Sepal and Petal width and height](output/fig/length-width.png)

# Table

*Table: Sepal and Petal width and height per Species, mean (standard deviation)*
