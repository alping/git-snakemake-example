import pandas as pd

infile = snakemake.input[0]  # "data/iris.csv"
csvfile = snakemake.output["csv"]  # "output/tbl/mean-std.csv"
mdfile = snakemake.output["md"]  # "output/tbl/mean-std.md"

table = (
    pd.read_csv(infile)
    .groupby("Species")
    .agg(["mean", "std"])
    .stack(level=0)
    # Combine mean and standard deviation into one cell
    .apply(lambda x: f"{x['mean']:.2f} ({x['std']:.2f})", axis="columns")
    .unstack()
)

# Export to CSV
table.to_csv(csvfile)

# Export to Markdown
with open(mdfile, "w") as f:
    table.to_markdown(f)
