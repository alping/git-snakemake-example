* Read the SAS Iris dataset and export it
* to a CSV file in the data folder;
PROC EXPORT DATA=Sashelp.Iris
    OUTFILE='data/iris.csv'
    DBMS=csv
    REPLACE;
RUN;
