# Make all files
rule all:
    input:
        "output/report.docx",
        "output/fig/dag.png",
        "output/fig/fdag.png",

# The final report is converted to docx format
rule report:
    input:
        report = "report/report.md",
        tbl="output/tbl/mean-std.md",
        fig="output/fig/length-width.png",
    output:
        "output/report.docx"
    shell:
        "pandoc -o output/report.docx {input.report} {input.tbl}"

# R script to create figures
rule create_figures:
    input: "data/iris.csv"
    output: "output/fig/length-width.png"
    script: "src/create_figures.R"

# Create a table using Python
rule create_table:
    input: "data/iris.csv"
    output:
        csv="output/tbl/mean-std.csv",
        md="output/tbl/mean-std.md",
    script: "src/create_table.py"

# Get the data using R
rule data_r:
    output: "data/iris.csv"
    script: "src/get_data.R"

# Get the data using SAS
# rule data_sas:
#     output: "data/iris.csv"
#     shell: "sas src/get_data.sas -nosplash -icon -log NUL"

# Visualize the workflow as a DAG
rule dag:
    output:
        "output/fig/dag.png"
    shell:
        "snakemake --rulegraph | dot -Gsize=5,5\! -Gdpi=300 -Tpng -o{output}"

# Visualize the workflow as a DAG with more information
rule fdag:
    output:
        "output/fig/fdag.png"
    shell:
        "snakemake --filegraph | dot -Gsize=5,5\! -Gdpi=300 -Tpng -o{output}"
