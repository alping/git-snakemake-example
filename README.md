# Git/Snakemake Example

An example project using Git for version control and Snakemake for workflow management.

## Requirements

- `Python`
  - `pandas` for data frames
  - `tabluate` for exporting markdown tables
- `R`
  - `dplyr` for data manipulation
  - `stringr` for string manipulation
  - `ggplot2` for plotting
  - `cowplot` for arranging plots in a grid
- `Snakemake`
- `Pandoc` for creating the final report
- (`Graphviz`, for visualizing the DAGs)
- (`SAS` for getting the data with SAS instead of R)
